/*
*
* filename: contentSwap.js
* author: jake rego
* description: This file will swap the content on the panel
*
*/

/* if the button is for the buyers */
$('.section--services').on('click', '#js-btn--buyers', function() {

  console.log("Click Detected");

  // fade out the old test
  $( '.panel__item' ).fadeOut( "slow", "linear" );

  // remove the alt class from the other button
  $('.btn--alt').removeClass('btn--alt');

  // add alt class to the active button
  $('#js-btn--buyers').addClass('btn--alt');
  
  // empty the panel
  $( '.panel__item' ).remove();

  // slide the triangle
  $( '.panel__triangle' )
    .removeClass( 'panel__triangle--right' )
    .addClass( 'panel__triangle--left' );

  // add icons and content
  $( '.panel' ).append(
    "<div class=\"panel__item\">"
    + "<div class=\"panel__item--logo glyph-before\" data-icon=\"&#xe907\"></div>" 
    + "<p class=\"panel__item--text\"><strong>Pre-approval.</strong>  I suggest working with a smaller, more personal bank to do your mortgage.  New laws such as TRID, as well as a busy housing market, can slow down banks that are busy managing checking and savings accounts.  Looking into programs that you might qualify for, such as grants for grads and first time homebuyer grants, can also be helpful in the home-buying process.  Sometimes banks will approve you for more than you would choose to buy a home for.  Spend some time putting together your personal finances, including your mortgage, taxes, insurance and any emergency funds you would want to have for new home.</p>"
    + "</div>"
    );

  $( '.panel' ).append(
    "<div class=\"panel__item\">"
    + "<div class=\"panel__item--logo glyph-before\" data-icon=\"&#xe908\"></div>" 
    + "<p class=\"panel__item--text\"><strong>Get on the MLS email system.</strong>  As a Realtor, I am able to get you set up to receive the most up to date listings that fit your criteria.  I am not the biggest fan of looking for active homes on Trulia, Zillow and other online sites since they do not have the most up to date information.</p>"
    + "</div>"
    );

  $( '.panel' ).append(
    "<div class=\"panel__item\">"
    + "<div class=\"panel__item--logo glyph-before\" data-icon=\"&#xe906\"></div>" 
    + "<p class=\"panel__item--text\"><strong>Once you like a home, do a drive by.</strong>  I can’t stress how important that is.  Online pictures are sometimes deceiving, you might love the look of a home online- but then realize driving by there is no back or side yard, or it is next door to an abandoned parking lot.   It’s hard to tell the exact positioning of the house on a lot by looking online.</p>"
    + "</div>"
    );

  $( '.panel' ).append(
    "<div class=\"panel__item\">"
    + "<div class=\"panel__item--logo glyph-before\" data-icon=\"&#xe90a\"></div>" 
    + "<p class=\"panel__item--text\"><strong>Call me for a showing!</strong> Once you find a home (or multiple homes) that you are interested in seeing, call me and we will set a time to view them.  You can call, email or text me which homes you would like to see, and times that work to go see them- I will set the appointments and confirmed with you when the Seller has approved the appointment.</p>"
    + "</div>"
    );

  // fade in
  $( '.panel__item' ).fadeIn( "slow", "linear" );
  console.log("Fade Done.");
});



/* if the button is for the sellers */
$('.section').on('click', '#js-btn--sellers', function() {

  //console.log("Click Detected");

  // fade out the old test
  $( '.panel__item' ).fadeOut( "slow", "linear" );

  // remove the alt class from the other button
  $('.btn--alt').removeClass('btn--alt');

  // add alt class to the active button
  $('#js-btn--sellers').addClass('btn--alt');
  
  // empty the panel
  $( '.panel__item' ).remove();

  // slide the triangle
  $( '.panel__triangle' )
    .removeClass( 'panel__triangle--left' )
    .addClass( 'panel__triangle--right' );

  // add icons and content
  $( '.panel' ).append(
    "<div class=\"panel__item\">"
    + "<div class=\"panel__item--logo glyph-before\" data-icon=\"&#xe905\"></div>" 
    + "<p class=\"panel__item--text\"> <strong>Clean & Organize!</strong> It is going to take a little elbow grease but it will pay off in the end.   You want potential buyers to feel like they are not walking into your home, but a home that they could make their own.  Go through each room and rid it of everything personal.  Most pictures should be taken down, toys put away, anything religious, political, or controversial should be removed while the house is being shown.  Ideally, you will not have to keep things this perfect for very long (aka- house goes into contract quickly!) but it’s worth starting out on a good note. Before you go on the market, consider: "  
    + "Getting all carpets professionally cleaned.  "
    + "Re-mulching yard/ sprucing up landscaping and outdoor lighting.  "
    + "Completing any projects around the house that need to be completed.  "
    + "Fresh coat of paint on any loud or unique colored walls.  "
    + "Consider if you home needs a professional stager to come in."   
    + "Make sure beds are made and bathrooms are cleaned.  "
    + "By completing this list to the best of your ability, you could add thousands of dollars to a potential buyers offer - it\’s worth all of the hard work you put into it!</p>  "
    + "</div>"
    );

  $( '.panel' ).append(
    "<div class=\"panel__item\">"
    + "<div class=\"panel__item--logo glyph-before\" data-icon=\"&#xe90c\"></div>" 
    + "<p class=\"panel__item--text\"><strong>Price your home.</strong> The price of your home is based off a multitude of factors including market price, current comparable and recently sold properties, and the buyer (s) that make the offer.  A common saying in the business is \“a house is only worth what someone is willing to pay for it\”.  For the most part, that is true, but we all know that there was more that goes into it.  The market will quickly tell you if you priced your home correctly or not- so getting it right the first time is important. We will study comparable properties in the neighborhood and create a listing price that will draw attention and lots of showings within that important first week on the market. </p>"
    + "</div>"
    );

  $( '.panel' ).append(
    "<div class=\"panel__item\">"
    + "<div class=\"panel__item--logo glyph-before\" data-icon=\"&#xe909\"></div>" 
    + "<p class=\"panel__item--text\"><strong>Showing your home and open houses.</strong>  Everyone has different comfort levels with people in your home.  The key to your house will be placed in a Supra lockbox near your front door.  A real estate agent can then make an appointment though a scheduling program called Centralized Showing to request to show your home.  Centralized Showing will call you confirming requested time, and you can choose to accept or deny (choose accept!!).   Open houses, in my opinion, are a personal decision.  Sunday open houses often produce a lot of neighbors and looki-loos, but also could produce your potential buyer.</p>"
    + "</div>"
    );

  $( '.panel' ).append(
    "<div class=\"panel__item\">"
    + "<div class=\"panel__item--logo glyph-before\" data-icon=\"&#xe90b\"></div>" 
    + "<p class=\"panel__item--text\"><strong>After a week on the market.</strong> Hopefully your home is in contract within the first week of going on the market, but if not, it’s time to revaluate.  We will need to go through the feedback we have received on the home and see if we need to make any changes to our gameplan.</p>"
    + "</div>"
    );

  // fade in
  $( '.panel__item' ).fadeIn( "slow", "linear" );
  //console.log("Fade Done.");


});




