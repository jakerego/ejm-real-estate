$(document).ready(function() {
  var stickyNavTop = $('.nav').offset().top;
   
  var stickyNav = function(){
  var scrollTop = $(window).scrollTop();
        
  if (scrollTop > stickyNavTop) { 
      // Add the js and styling classes
      $('.nav').addClass('js-nav--sticky');
  } else {
      // Remove the js and styling classes
      $('.nav').removeClass('js-nav--sticky');
  }
  };
   
  stickyNav();
   
  $(window).scroll(function() {
      stickyNav();
  });
});