/*
 * filename: dynamic.js
 */

$(document).ready( function() {

  /* change size initially onload */
  $('.nav__large').css('height', $('.nav__small').height() + 6 );  

  /* change size on resize */
  $(window).resize( function() {
    $('.nav__large').css('height', $('.nav__small').height() + 6 );
  });

});