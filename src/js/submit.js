/*
*
* @filename: submit.js
* @author: Jake Rego
* @description: This file will check the email initially for errors
*               and pull up the modal.
*/

$('.form').on('click', '.js-form__btn', function(e) {
  // Will not propogate event handler (i.e. page will not reload)
  e.preventDefault();

  var re = /[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}/igm; // regex variable
  var category = $('.form__select').val();
  var email    = $('.form input[name="email"]').val();
  var name     = $('.form input[name="name"]').val();
  var message  = $('.form textarea[name="inquiry"]').val();

  var modalHeader   = $('.modal h2');
  var modalContent  = $('.modal p');
  var modalOverlay  = $('.moday-overlay');

  var failedHeader    = "Oops...something went wrong.";
  var failedContent   = "Please try submitting that again.  We apologize for the inconvenience.";
  var successHeader   = "Thank you for reaching out to me!";
  var successContent  = "I've received your email and will reach out as soon as I can to help to help you with your housing endeavors.";

  var windowHeight = $(document).height();

  var contentType ="application/x-www-form-urlencoded; charset=utf-8";

  // /* email validation */
  // // check for artist/listener selection - if empty, alert and do nothing
  if( $('.form__select').val() == null) {
    $('#form__feedback--select').css('display', 'block');
    console.log("category null");
  } 
  else if( $('.form input[name="email"]').val().length() === 0) {
    $('#form__feedback--input--email').css('display', 'block');
    console.log("email null");
  } 
  else if( $('.form input[name="name"]').val().length() === 0 ) {
    $('#form__feedback--input--name').css('display', 'block'); 
  } 
  else if( $('.form textarea[name="inquiry"]').val().length() === 0 ) {
    $('#form__feedback--input--inquiry').css('display', 'block');
  } 
  else {
    // clear the feedback boxes so no errors are displayed
    $('#form__feedback--input--email').css('display', 'none');
    $('#form__feedback--input--name').css('display', 'none');
    $('#form__feedback--input--inquiry').css('display', 'none');
    $('#form__feedback--select').css('display', 'none'); 

    
    // if valid - pull open modal and submit!
    if( re.test(email) ) {
      console.log("CATEGORY: " + category.toString());

      /* send the email to Emily */  
      $.post("mail.php", { 
          email:    email.toString(), 
          category: category.toString(),
          name:     name.toString(),
          message:  message.toString()
      }, 

      function( data ) {
        console.log(data);
        /* show the success modal */
        $('.modal--overlay').css('height', '\'' + windowHeight + 'px' + '\'');
        modalHeader.text( successHeader );
        modalContent.text( successContent );
      })
      .fail(function() {
        // add 'failed' modal
        modalHeader.text( failedHeader );
        modalContent.text( failedContent );
      })
      .always( function() {
        $('.modal--overlay').css('display','block');
        $('.modal').css('display', 'block');
      });
      /* END MAIL CALL POST */

    } /* end testing email IF block*/
    else {
      alert("Please enter a valid email address.");
    }
  } /* else */
});
