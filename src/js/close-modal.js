/*
* @filename: close-modal.js
* @author: Jake Rego
* @desctiption: This file applies the necessary CSS to "close" the modal
*/


/* close the modal if you click the close button */
$('.modal').on('click', '.js-modal__close', function() {
  $('.modal').css('display', 'none');
  $('.modal--overlay').css('display', 'none');
});


/* close the modal when you click outside of it */
$('.modal').on('click', '.modal--overlay', function() {
  $('.modal').css('display', 'none');
  $('.modal--overlay').css('display', 'none');  
});