<?php

require 'PHPMailer/PHPMailerAutoload.php';
/* set the config "from" value */
ini_set ( 'sendmail_from' , 'website@realcolumbusestate.com' );

/* Grab/set variables for the email */
$email            = $_REQUEST['email'];           // email from the form
$category         = $_REQUEST['category'];        // category of the form
$name             = $_REQUEST['name'];            // name of the person from the form
$message          = $_REQUEST['message'];         // message from the form
$mail_to_address  = 'testjakerego@gmail.com';//"emead@rzrealty.com";  // all of these are going to Emily

/* configure the PHP Mailer email */
$mail = new PHPMailer;
$mail->isSMTP();                                      // Set mailer to use SMTP
$mail->Host = 'mail.realcolumbusestate.com';          // Specify main and backup SMTP servers
$mail->SMTPAuth = true;                               // Enable SMTP authentication
$mail->Username = 'website@realcolumbusestate.com';            // SMTP username
$mail->Password = 'b4lls4ck';                     // SMTP password FOR REALCBE
$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
$mail->Port = 25;                                     // TCP port to connect to
$mail->isHTML(false);                                  // Set email format to HTML
$mail->setFrom( 'website@realcolumbusestate.com', 'Website Inquiry'); // This email is coming from the website itself
$mail->addAddress($mail_to_address);                  // Add Emily a recipient
$mail->addReplyTo( 'website@realcolumbusestate', 'Website Inquiry' );                   // add the requestor as the reply to
$mail->Subject = 'Potential ' . $category . ', ' . $name;  // Subject = "Potential [buyer/seller], [name]"

$mail_message = "New Inquiry from: " . $name .
                ", who is a potential " . $category .
                ".  \r\n" .
                "\r\nTheir email address is: " . $email . "\r\n" .
                $name . " says: \r\n" .
                $message;

$mail->Body    = $mail_message;
/**********************************/

// Set up the array to check category against
$check_these = array('buyer', 'seller');

// check if (somehow) the category isn't included, throw an error.
if( !in_array( $category, $check_these ) ) {
  echo "Invalid Category.";
  exit;
}

// check if the email address is valid
if( !filter_var($email, FILTER_VALIDATE_EMAIL) ) {
  echo "Invalid Email.";
  exit;
}

/* attempt to send the email */
if(!$mail->send()) {
  echo 'Message could not be sent.';
  echo 'Mailer Error: ' . $mail->ErrorInfo;
  $error_message = "\r\nERROR: [" . date(DATE_RFC2822) . "] User not added: ". $email . "Role: " . $category;
  file_put_contents("log.txt", $error_message, FILE_APPEND);
} 
else {
  echo 'Message has been sent';
}

?>